#!/bin/sh

gcc -Os -g tests/main.c -Inanoprof -o test
gcc -O3 -g tests/main.c -Inanoprof -o test-O3
gcc -O2 -g tests/main.c -Inanoprof -o test-O2
gcc -g tests/main.c -Inanoprof -o test-debug

Nanoprof stream format
======================

Nanoprof 'stream' is a tightly packed sequential stream of "events", sometimes
interspersed by padding bytes. Type of event is specified by a variable-length
'bit prefix' of its first byte. Often-used events use shorter prefixes so they
can store data in remaining bits of the first byte.

Events
======

Timepoint: `0b0`
---------------

Most common event - a 'timepoint ID' identifying a code location (e.g.
start/end of a function) followed by a timestamp.

Each ID must be registered by a previous `ID registration` event (prefix
`0b11111110`), which specifies the name / nest level of the code location.

Time is a delta relative to previous timepoint. 'Clock rate' is arbitrary and
can vary over time (e.g. depending on CPU frequency) and is calculated (in
clocks per nanosecond) by:

```
t2 - t1
-------
h2 - h1
```

`h1` and `h2` are timestamps of successive `high precision time events`
(prefix `0x11111101`) in nanoseconds, `t1` and `t2` are timestamps
(calculated by adding up all time deltas in between) of time point events
immediately after respective high precision events. 

Time measurement is fast but unreliable: time may 'skip' when measuring
thread is moved between CPUs. Such skips are corrected by updating 'current
time' at each high precision event (starting at the next timepoint event).

**Format**:

Timepoint events have three formats (2 byte, 4 byte, 8 byte) based on the ID
and time delta size, to save as much memory as possible.

**2 byte format**: `0iiiiiii 0ttttttt`

Used when: timepoint ID fits into 7 bits, time delta fits into 7 bits

* `iiiiiii` is a 7-bit timepoint ID
* `ttttttt` is a 7-bit time delta.

For example, on a 3Ghz x86 CPU, a 'time unit' corresponds to 16 cycles or
5.33ns and this can be used for time deltas up to 677ns.

**4 byte format**: `0iiiiiii 1iiiiiii 0ttttttt tttttttt`

Used when: timepoint ID does not fit into 7 bits OR time delta needs >7 bits
but fits into 15 bits. The OR is non-exclusive.


* `iiiiiii iiiiiii` (extracted from 2 bytes) is a 14-bit timepoint ID.
* `ttttttt tttttttt` is a 14-bit time delta.

Both values are in big endian byte order.

For example, on a 3Ghz x86 CPU, a 'time unit' corresponds to 16 cycles or
5.33ns and this can be used for time deltas up to 174us.

**8 byte format**: `0iiiiiii 1iiiiiii 1ttttttt tttttttt tttttttt tttttttt tttttttt tttttttt`

Used when: time delta does not fit into 15 bits.

* `iiiiiii iiiiiii` (extracted from 2 bytes) is a 14-bit timepoint ID.
* `ttttttt tttttttt tttttttt tttttttt tttttttt tttttttt` is a 47-bit time delta.

Both values are in big endian byte order.

For example, on a 3Ghz x86 CPU, a 'time unit' corresponds to 16 cycles or
5.33ns and this can be used for time deltas up to 8 days and 16 hours. Which
ought to be enough for everybody.

High precision time: `0b11111101` aka `0xFD`
--------------------------------------------

High-precision time measurement in nanoseconds. Sets the time value of the
next timepoint, and calibrates timepoint clock rate. This limits
errors caused by CPU thread jumping and frequency throttling.

**Format**: `11111101 tttttttt tttttttt tttttttt tttttttt tttttttt tttttttt tttttttt`

Stores an exact 56-bit nanosecond timestamp in big endian byte order. Enough
for 2 years.

Data event: `0b1010` aka `0xA`
------------------------------

Stores a single value of various data types (useful for high-frequency
logging of stats that may affect performance).

NOTE: this event is in very early stages and will almost certainly see
format breaking changes.

**Format**: `0b1010tttt dddddddd ... dddddddd`

* `tttt`: data type. Determines how to interpret the data bytes.
  Can be one of:

  * `0x1`: `uint8_t` - single data byte.
  * `0x2`: `uint16_t` - 2 data bytes, native endian.
  * `0x3`: `uint32_t` - 4 data bytes, native endian.
  * `0x4`: `uint64_t` - 8 data bytes, native endian.
  * `0x5`: `float` - 4 data bytes, native float format.
  * `0x6`: `double` - 8 data bytes, native double format.
  * `0x7-0xE`: not used yet. (ideas: signed ints? 128-bit? half-float?)
  * `0xF`: `string` - any number of data bytes, terminated by a zero (`0x00`) byte.
* `dddddddd ... dddddddd`

  Byte or bytes storing the value. Size depends on data type, and if type is
  string, can be arbitrary (strings are zero-terminated).


Timepoint event ID registration: `0b11111110` aka `0xFE`
--------------------------------------------------------

Specifies name and nest level for a timepoint ID.  Must precede any timepoint
ID with said ID.

The name is not stored directly in the event - it is stored in a string data
event that follows immediately after the registration event.

**Format**: `1111_1110 llll_llll 00ii_iiii iiii_iiii`, immediately followed by a Data event with type string.

* `llll_llll` is the level as a single byte.
* `00ii_iiii__iiii_iiii` is the 14-bit timepoint ID.


**Notes on level nesting**

Higher levels nest inside lower levels. This means that the time scope aka
'zone' started by a timepoint ends at the first event with the same or
*lower* level. **Exception**: Level -128 is used for internal nanoprof
profiling. Zones at lower levels are not ended by level -128 timepoints.

**Special / builtin timepoint ID ranges**

These are registered immediately when nanoprof is initialized.

* `0x3C00` - `0x3CFF`: 'End time scope' event IDs for each level: from -128
  (0x3C00) to +127 (0x3CFF). These timepoints indicate the end of the
  time scope started by the last timepoint event at the same level. 
  A timepoint event's time span always ends with the next event at the
  same level (to avoid requiring two time measurements for each 'scope')
  but sometimes we want to end the scope without starting another one.
* `0x3D00` - `Return buf` time event. A self-measurement level -128 timepoint
  event used for self-measurement of the `return_buf` callback call.

Padding byte: `0b11111111` aka `0xFF`
-------------------------------------

When nanoprof lacks enough buffer space to write the next event, it fills the
rest of the buffer with 0xFF bytes and flushes.  This also ensures each
bufferful in dumped stream starts at a multiple of buffer size, which can be
used for radom access or parallel processing.  These bytes should be skipped. 

**Format**: `0b11111111`

Remaining free event prefixes
=============================

- `0b11` except for `0b11111111`,`0b11111110`, `0b11111101`

   Space for up to 61 new event types.
- `0b100`

   Space for up to 32 new event types
- `0b1011`

   Space for up to 16 new event types


Places where specific events are written to the stream
======================================================

* Nanoprof works by writing emitted events into a buffer and flushing the buffer
  when full. Each buffer has a fixed size and may be padded to this size with
  padding bytes (`0xFF`).
* A high precision time event is written at the beginning of each buffer.
* The last buffer written by the profiled program is ended by a high precision
  time event, allowing to calculate clock rate for the last timepoint events.

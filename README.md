nanoprof
========

Minimalist real-time instrumenting tracing profiler library for C/C++.

Very early stages - do not use yet unless you're OK with rewriting your code.


Features
--------

* Implemented as a single C header
* Written in C11 for compatibility with both C and C++
* *Tracing* profiler: records each instance of profiled events to view a timeline of process execution
* Manual instrumentation
* Records user-specified variables
* Output can be viewed by Chrome `about://tracing`
* Low memory overhead (can go as low as 2B per recorded event)
* Low runtime overhead (measures time once per scope instead of twice by default, precision on the order of 10ns)
* Only works on GCC/Clang on x86/AMD64 **at the moment**
* Mimimalist, well documented code
* Memory allocation and output can be controlled by the user
* Faster than alternatives at the cost of a less user-friendly interface
  ( need to specify an event's name/nest level before using it )
* No hidden, global state (but user must manage per-thread profiler contexts)

Planned features
----------------
* Cross-compiler and cross-platform support
* SSH-friendly real-time TUI frontend
* Real-time profiling over network


Getting Started
---------------

1. `git clone git@gitlab.com:v01d-sk/nanoprof.git`
2. Copy file `nanoprof/nanoprof.h` into your project or add its containing
   directory as an include path.
3. Add instrumentation to your program. Example:

```c
#include <nanoprof.h>

/// Timepoint IDs used.
enum TIMEPOINT_ID
{
    TIMEPOINT_PROGRAM = 0,
    TIMEPOINT_ACTIVITY,
    TIMEPOINT_OTHER_ACTIVITY
};

int main()
{
    FILE* file_prof = fopen("output.nanoprof", "w");
    if( !file_dump )
    {
        return 1;
    }

    const size_t buffer_size = 1024 * 1024;
    // Use `nanoprof_create_advanced` to gain full control over allocation and
    // to support different than FILE* output
    struct nanoprof prof = nanoprof_create( buffer_size, file_prof );

    // Register profiled events' ID's, names and nest levels.
    // Unwieldy but necessary to minimize amout of recorded data.
    // Events at lower nest levels end scopes of events at higher or equal nest levels.
    // Parameters: struct nanoprof, ID, scope nest level, name
    nanoprof_event_register( &prof, TIMEPOINT_PROGRAM,        0, "program" );
    nanoprof_event_register( &prof, TIMEPOINT_ACTIVITY,       1, "profiled_activity" );
    nanoprof_event_register( &prof, TIMEPOINT_OTHER_ACTIVITY, 1, "other_profiled_activity" );

    // Starts a level 0 scope for TIMEPOINT_PROGRAM.
    nanoprof_event_timepoint( &prof, TIMEPOINT_PROGRAM );
    for( size_t i = 0; i < 42; ++i )
    {
        // Starts a level 1 scope for TIMEPOINT_ACTIVITY.
        // Does *not* end scope of TIMEPOINT_PROGRAM since the level is higher.
        nanoprof_event_timepoint( &prof, TIMEPOINT_ACTIVITY );
        profiled_activity();
        // Ends the scope of TIMEPOINT_ACTIVITY and starts one for TIMEPOINT_OTHER_ACTIVITY
        nanoprof_event_timepoint( &prof, TIMEPOINT_OTHER_ACTIVITY );
        other_profiled_activity();
    }
    // Ends the scopes of TIMEPOINT_PROGRAM *and* the last TIMEPOINT_OTHER_ACTIVITY
    nanoprof_event_timepoint_end( &prof, TIMEPOINT_PROGRAM );

    // Clean up.
    nanoprof_destroy( &prof );
    fclose( file_dump );
}
```


4. Convert the output for Chrome's `about://tracing` (custom viewer is WIP)
   using the utility script `nanoprof2chrome.d`

   `chmod +x ./util/nanoprof2chrome.d`

   `cat output.nanoprof | ./util/nanoprof2chrome.d > output.json`

5. Open ``about://tracing`` in Chrome and open file `output.json`.
   This will take forever for non-trivial outputs - that's why we need
   a custom viewer.


See the [API reference](https://gitlab.com/v01d-sk/nanoprof/-/blob/master/doc/nanoprof.pdf)
for more information.

The reference is in PDF format, you can also generate HTML yourself with doxygen:

```
doxygen Doxyfile
```

which will generate documentation in the `doc/html` directory.

Tracing profilers
-----------------

Most profilers give you an **overall overview** on where most time is spent.
This can be done by counting how often each function is visited, repeatedly
interrupting a process to sample where it is, etc. At the output you will
likely get a statistical breakdown telling you that **X% of time is spent in
function Y**, maybe with a call graph.

**Tracing profilers** like `nanoprof` record *every instatiation* of profiled
code separately. E.g. every function call, every loop iteration, or whatever
else you need to measure. This is generally more performance-intensive and
*much* more memory-intensive than a traditional profiler, but allows you to
observe your program in much more detail.

A traditional profiler will tell you that `std::sort` is taking up 30% of time.
A tracing profiler will tell you that `std::sort` is usually almost instant,
but on its 24th call it suddenly takes forever - and even allows you to record
info about parameters that caused this to happen.

Unlike with most profilers, a tracing profiler generally cannot afford to
record every single function - requiring the user to manually instrument the
code. In case of `nanoprof` this is done with **timepoint** events.

Design philosophy
-----------------

Most tracing profilers, like `optick`, `MicroProfiler` or `remotery`, rely on
the user to insert function calls or C++ RAII classes to record *events*
marking the beginning and end of profiled activity, passing some sort of name
to describe the scope.  When the activity is entered/exited, a timestamp is
recorded together with the name.

This is easy to use as it allows the user to simply insert a function call or
macro at any place, without much initialization / setup. If you are profiling a
game or a similar application working with millisecond-level latencies, this is
probably the best approach and such tools will be a better fit than `nanoprof`.

`nanoprof` is targeted at profiling multi-gigabit packet processing code, where
we often work with microsecond or multi-nanosecond latencies. This forces to
sacrifice user friendliness for **absolute minimum overhead achievable**.

`nanoprof` achieves high performance thanks to the following observarions:

1. *time measurement is expensive*: Commonly-used high-precision timers (such as
   those in the C++ `std::chrono`) have high overhead, which can drastically
   skew profiling results.

2. *memory is expensive*: Writing a 64bit timestamp value, scope information
   and a string for each event quickly eats through memory, using up memory
   bandwidth and eventually leads to I/O overhead when we need to write or send
   the profiled data.

3. *most tracing profilers do double the work required*: Usually both the
   beginning and the end of an activity (scope) are recorded. But in tight
   loops (which record the majority of events), *end* of an iteration is
   immediately followed by the *beginning* of the next. It should be possible
   to measure time only once in this case.

These lead to the following implementation decisions:

1. `nanoprof` uses fast, but less precise/reliable ways to measure time, e.g.
   the `rdtsc` instruction on x86/AMD64. This still skews the results due to
   imprecision, but this is less significant than standard timers.  This is not
   particularly uncommon - most of the better tracing profilers use such fast
   timing instructions.
2. `nanoprof` requires the user to *register* profiled events, associating
   a *timepoint* ID (event ID) with a name and nest level. When profiled
   activity is entered, only this ID is written out, which uses up 1 or 2 
   bytes depending on ID value.

   Furthermore `nanoprof` records time in *deltas*, and uses less bytes if the
   delta value is small enough (1, 2 or 6 bytes can be used).

   Thus, in most dense loops, `nanoprof` will only write 2 bytes per event (the
   more dense a loop is, the smaller are the deltas).

3. `nanoprof` does not internally think in terms of *scopes* - those are only
   generated by post-processing recorded data. In a loop, a *timepoint* event
   only has to be added at the beginning of an iteration. This will result in a
   scope that will end at the beginning of the next iteration, or at the
   nearest event after the scope. Special 'end' event can be recorded
   immediately after a loop or any other activity we need to explicitly end.

   Events registered with lower nest levels end scopes for events with higher
   or equal nest levels. 

For more details, see:

* `FORMAT.md` for the (unstable!) nanoprof event stream format specification.
* `nanoprof/nanoprof.h` for nanoprof implementation
* `util/nanoprof2chrome.d` for how nanoprof events can be turned into scopes
  viewable by Chrome `about://tracing`.

License
-------

```
         Copyright Ferdinand Majerech 2020.
Distributed under the Boost Software License, Version 1.0.
   (See accompanying file LICENSE_1_0.txt or copy at
         http://www.boost.org/LICENSE_1_0.txt)
```

Similar Projects
----------------

* [MicroProfiler](https://github.com/tyoma/micro-profiler)
* [C++ Rapid Profile](https://github.com/grant-zietsman/rapid-profile)
* [Remotery](https://github.com/Celtoys/Remotery)
* [easy_profiler](https://github.com/yse/easy_profiler)
* [optick](https://github.com/bombomby/optick)
* [Telemetry (closed source)](http://www.radgametools.com/telemetry.htm)
